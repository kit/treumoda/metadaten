# TreuMoDa

## Definition „Mobilitätsdaten“

Es existiert noch keine allgemeinverbindliche Definition, welche Daten
und Datenkategorien unter den Begriff der „Mobilitätsdaten“ fallen
können. Allgemeinhin bezieht sich „Mobilität“ regelmäßig auf den
öffentlichen Straßenverkehr und umfasst somit sowohl Individualfahrzeuge
als auch den Personen- und Güterverkehr, Schienenverkehr,
Elektrokleinstfahrzeuge, Drohnen bis hin zu Mobilitätsplattformen. Die
*Verbraucherzentrale Bundesverband* (vzbv) definiert „Mobilitätsdaten“
in seiner Position als „*Fahrzeugdaten sowie alle weiteren durch
Mobilität anfallende Daten und Daten der Infrastruktur*“.[^1]

## Systematisierung Mobilitätsdaten

Die Studie „‘Eigentumsordnung‘ für Mobilitätsdaten?“ im Auftrag des BMVI
kategorisiert Mobilitätsdaten als: statische (nichtveränderliche)
Fahrzeugdaten, dynamische Fahrzeugdaten (insbes. Sensor- und
Diagnoseergebnisdaten), Verkehrslagedaten, dynamische
(Mobilitäts-)Nutzerdaten (konkrete Daten zum Fahr-/Mobilitätsverhalten),
statische Nutzerdaten und Positionsdaten (ortunveränderlich, wie bspw.
Parkplätze und dynamische, wie bspw. Standorte von Fahrzeugen).[^2]

Im Hinblick auf die datenschutzrechtliche Bewertung bestehen erhebliche
Unterschiede, ob es sich um statische/dynamische Fahrzeugdaten, welche
einem individuellen Fahrzeug und damit regelmäßig auch einer oder
mehrerer individualisierbaren natürlichen oder juristischen Personen
zugeordnet werden können, Infrastruktur- und Verkehrsdaten oder
sonstige, anonymisierte oder aggregierte Mobilitätsdaten bspw. aus dem
Kontext der Personenbeförderung handelt. Unter Berücksichtigung des
gesamten Mobilitätssektors ließen sich „Mobilitätsdaten“ daher zunächst
in drei Gruppen strukturieren: Daten zu einzelnen Fahrzeugen
(Fahrzeugdaten), Daten zu Verkehr und Infrastruktur (Verkehrsdaten)
sowie Daten zur Personen- und Güterbeförderung (vgl.
Mobilitätsdatenverordnung).

#### Fahrzeugdaten

Typische beim Fahrtbetrieb generierte Daten lassen sich in folgende
Kategorien einteilen:

- **Technische Daten** zum Fahrzeug, Fahrtbetrieb und Fahrzeugzustand
  (insbesondere Position, Geschwindigkeit, Beschleunigung, etc.), Daten
  für Wartungs- und Reparaturzwecke,

  - „Offline-Diagnose“: Auslesbar über OBD-Schnittstelle

  - „Online-Diagnose“: Datentransfer idR zum Fahrzeughersteller/ggf.
    Dienstleister

- **Daten zum Zustand der Fahrer\*in sowie Mitfahrer\*innen** (Systeme
  zur Detektion der Fahrerverfügbarkeit, Sitzbelegung, ggf. Anmeldedaten
  über Infotainment, etc.)

- **Kommunikationsdaten**: eCall, Car-2-Car- bzw. Car-2-X-Kommunikation
  (Datenaustausch zwischen Fahrzeugen und Verkehrsinfrastruktur),
  Vernetzung bzgl. Internetnutzung

  - Car-2-X: Cooperative Awareness Messages (CAM)[^3] und Decentralised
    Enviromental Notification Messages (DENM)[^4]

- **Daten zur Unfallrekonstruktion**: Event Data Recorder (Art. 6 Abs. 4
  General Safety Regulation[^5]), Fahrmodusspeicher (§ 1g StVG, § 63a
  StVG, UNECE-Regelung 157)

- **Umfelddatenerfassung** (bei automatisierten / autonomen Fahrzeugen
  i.d.R. mittels Radar, Lidar und Videokameras)

- **Identifikationsdaten:** Kfz-Kennzeichen, Fahrzeug-ID (FIN),
  Vertrags-/Kundennummern, etc.

#### Verkehrsdaten

Als verkehrsbezogene Daten können Daten zur Verkehrsinfrastruktur, zum
Verkehrsaufkommen und -leistung bis hin zu Informationen über das
tägliche Mobilitätsverhalten der Verkehrsteilnehmenden gefasst werden.
Mit Blick auf Zugangspunkte und Schnittstellen lassen sich exemplarisch
folgende auflisten:

- **Daten des Verkehrsmanagements** („Intelligente Verkehrssysteme“) wie
  bspw. Echtzeit-Verkehrsinformationen,
  straßenverkehrssicherheitsrelevante Verkehrsmeldungen, multimodale
  Reise-Informationsdienste, Informationen zur Verfügbarkeit von
  Parkplätzen,

- **Daten vernetzter Infrastrukturen**, wie bspw. per Road Side Units
  (RSU) an Verkehrsteilnehmende versendete Phasen von
  Lichtsignalanlagen, Zustände von Schranken, lokale Karten,

- **Event-gesteuerte Informationen** zur Verkehrslage wie bspw.
  Ankündigungen von Baustellen, Hinweise auf Verkehrsstörungen,
  Gefahrensituationen und Hindernisse,

- **Daten sonstiger öffentlicher Infrastrukturen**, die (noch) nicht
  vernetzt sind,

- **Verkehrsbarometer** automatischer Dauerzählstellen,

- **Daten zum Mobilitätsverhalten,** bspw. Positionsdaten von
  Datenlieferanten wie Verleihdiensten, Navigationsdiensten, etc.

Weitere verkehrsrelevante Daten sind Wetterdaten sowie sonstige
Umweltdaten und im Bereich der E-Mobilität Informationen zu
Ladestationen.

#### Mobilitätsdatenverordnung

Die Mobilitätsdatenverordnung (MDV) enthält keine Definition des
Begriffs „Mobilitätsdaten“. Die Regelung erfasst folgende in Anlage zu §
1 Nr. 1, §§ 3 und 5 Abs. 1 MDV aufgeführten Daten:

- Daten im **Zusammenhang mit der Beförderung von Personen**

  - im **Linienverkehr**

    - statische Daten: Unternehmer/Vermittler, Fahrpläne, Routen,
      Tarifstrukturen/Preise, Buchungs- und Bezahlmöglichkeiten, Daten
      zum Umweltstandard und der Barrierefreiheit der eingesetzten
      Fahrzeuge

    - dynamische Daten: Ausfälle, Störungen, Verspätungen, vorrauss.
      Abfahrts-/Ankunftszeit, tatsächliche oder prognostizierte
      Auslastung

  - im **Gelegenheitsverkehr**

    - statische Daten: Unternehmer/Vermittler, Bediengebiet und -zeiten,
      Preise/beförderungsentgelte, Buchungs- und Bezahlmöglichkeiten,
      Daten zum Umweltstandard und der Barrierefreiheit der eingesetzten
      Fahrzeuge

    - dynamische Daten: tatsächlich abgerechnete Kosten, Verfügbarkeit
      von Fahrzeugen im Verkehr in Echtzeit, Auslastung

- Daten zu **Zugangsknoten und deren Infrastruktur im Linien- und
  Gelegenheitsverkehr** (Zugangsknoten, Infrastruktur an Zugangsknoten,
  aktueller Betriebsstatus der Zugangsknoten und von dort vorhandener
  Infrastruktur)

## Potentielle Datenquellen und einschlägige Rechtsnormen

### Fahrmodusspeicher

Mit der UNECE-Regel R 157 zu Automated Lane Keeping Systems (ALKS) wird
der sogenannte „Fahrmodusspeicher“ eingeführt (Data Storage System for
Automated Driving- DSSAD). Die Pflicht zur Ausrüstung gilt nach Nr. 8.1
„unbeschadet nationaler und regionaler Rechtsvorschriften über den
Zugang zu Daten, den Schutz der Privatsphäre und den Datenschutz.“

Im StVG ist in § 1g StVG eine vergleichbare Regelung für Kraftfahrzeuge
mit autonomer Fahrfunktion vorgesehen. Die Umsetzung der UNECE R-157
erfolgt in §§ 63a, 63b StVG.

#### Autonome Fahrzeuge

§ 1g StVG verpflichtet den Halter eines Fahrzeugs mit autonomer
Fahrfunktion i.S.d. § 1d StVG eine in Absatz 1 aufgestellte Liste an
Daten zu den in Absatz 2 genannten Anlässen aufzuzeichnen und zu
speichern. Gemäß Absatz 5 ist das Kraftfahrt-Bundesamt berechtigt beim
Halter erhobene nicht personenbezogenen Daten für verkehrsbezogene
Gemeinwohlzwecke, insbesondere zum Zweck der wissenschaftlichen
Forschung im Bereich der Digitalisierung, Automatisierung und Vernetzung
sowie zum Zweck der Unfallforschung im Straßenverkehr, folgenden Stellen
zugänglich zu machen:

1.  Hochschulen und Universitäten,

2.  außeruniversitäre Forschungseinrichtungen,

3.  Bundes-, Landes- und Kommunalbehörden mit Forschungs-,
    Entwicklungs-, Verkehrsplanungs- oder Stadtplanungsaufgaben.

Die genannten Stellen dürfen die Daten ausschließlich für die in Satz 1
genannten Zwecke verwenden. Setzt der Halter seinerseits Beschäftigte
gemäß § 26 BDSG als Technische Aufsicht ein, findet § 26 BDSG Anwendung.
Allgemeine Übermittlungsvorschriften bleiben unberührt.

Ausnahmen gelten bei Kraftfahrzeugen im Sinne des § 1k StVG, wobei es
sich um Kraftfahrzeuge für militärische, nachrichtendienstliche oder
polizeiliche Zwecke, für Zwecke der Zollfahndung, des Zivil- oder
Katastrophenschutzes, der Brandbekämpfung, der Straßenbauverwaltung oder
der Rettungsdienste handelt.

#### Automatisierte Fahrzeuge

UNECE-R 157 zu ALKS regelt die vom Fahrmodusspeicher (DSSAD) erfassten
Vorfälle (Aktivierung / Deaktivierung des Systems,
Übernahmeaufforderung) sowie die zu speichernden Datenelemente
(Vorfalls- und Zeitangaben). Bezüglich der Zugänglichkeit der Daten wird
auf nationale und regionale Rechtsvorschriften verwiesen. Daneben müssen
Die im Fahrmodusspeicher gespeicherten Daten in standardisierter Form
über eine elektronische Kommunikationsschnittstelle, zumindest über die
Standardschnittstelle (OBD-Anschluss), leicht ablesbar sein.

§ 63a StVG ergänzt die vom DSSAD zu erfassenden Datenelemente um die
Positionsangaben. Gemäß § 63a Abs. 1 STVG dürfen „im Zusammenhang mit
einem in § 7 Abs. 1 geregelten Ereignis \[…\] die gemäß Absatz 1
gespeicherten Daten in anonymisierter Form zu Zwecken der
Unfallforschung an Dritte übermittelt werden.“ Details zur technischen
Ausstattung, den Speicherort, Art und Weise der Speicherung, Adressat
der Speicherpflicht sowie Maßnahmen zu Sicherung der gespeicherten Daten
gegen unbefugten Zugriff bei Verkauf des Kraftfahrzeugs soll eine
Rechtsverordnung klären.

Umstritten ist, ob die DSGVO die Regelung inhaltlich vollständig oder
teilweise überlagert und diese folglich unangewendet bleiben muss.[^6]
Für die Datenverarbeitung zu Beweiszwecken könnte als Öffnungsklausel
Art. 6 Abs. 1 Buchst. c) DSGVO dienen,[^7] bei Ahndung von
Verkehrsverstößen wäre die JI-RL (RL 2016/680/EU) einschlägig (vgl. Art.
2 Abs. 2 Buchst. d) DSGVO).[^8] Aufgrund der fehlenden Regelung zur
Verantwortlichkeit dürfte die Vorschrift dem Konkretisierungsgebot des
Art. 6 Abs. 3 DSGVO nicht hinreichend Rechnung tragen.[^9] Erhebliche
Bedenken ruft zudem die vorgesehene Speicherdauer vor, welche mit den
Rechtsgedanken der Vorratsdatenspeicherung unvereinbar erscheint.[^10]

### Unfalldatenspeicher (Event Data Recorder)

Art. 6 Abs. 1 Buchst. g) VO (EU) 2019/2144 (General Safety Regulation –
GSR) verpflichtet Fahrzeuge ab Juli 2022 mit einem hochentwickelten
Fahrassistenzsystem zur ereignisbezogenen Datenaufzeichnung
ausgestatten. Die Datenaufzeichnung bezieht sich kurz vor, während und
unmittelbar nach einem Zusammenstoß und soll innerhalb eines
geschlossenen Systems erfolgen (Art. 6 Abs. 4 GSR). Die gesammelten
Daten sollen anonymisiert und vor Manipulation und missbräuchlicher
Verwendung geschützt gespeichert werden und gleichzeitig die
Identifizierung des genauen Fahrzeugtyps, der Version und der Variante
und insbesondere der im Fahrzeug eingebauten aktiven Sicherheits- und
Unfallvermeidungssysteme ermöglichen. Die ereignisbezogene
Datenaufzeichnung darf nicht in der Lage sein, die letzten vier Ziffern
des fahrzeugunterscheidenden Teils der Fahrzeug-Identifizierungsnummer
oder sonstige Informationen, die eine Identifizierung des einzelnen
Fahrzeugs, des Eigentümers oder des Halters ermöglichen könnten,
aufzuzeichnen und zu speichern (Art. 6 Abs. 5 GSR).

Die Daten können den nationalen Behörden auf der Grundlage des
Unionsrechts oder des nationalen Rechts im Einklang mit der DSGVO
ausschließlich für den Zweck der Unfallforschung und ‐analyse,
einschließlich für die Zwecke der Typgenehmigung von Systemen und
Bauteilen, über eine Standardschnittstelle zur Verfügung gestellt werden
(Art. 6 Abs. 4 Buchst. d) GSR).

### Verkehrsdaten

*IVS-Richtlinie:* Im Hinblick auf Verkehrsmanagementdaten sowie Daten
vernetzter Infrastrukturen wurde auf EU-Ebene ein regulatorischer Rahmen
unter besonderer Berücksichtigung der Aspekte der Kompatibilität,
Interoperabilität und Kontinuität für die Einführung „intelligenter
Verkehrssysteme“ erarbeitet.[^11] EU-Mitgliedstaaten sind verpflichtet,
einen „Nationalen Zugangspunkt“ für Mobilitätsdaten einzurichten.

*C-IST-Verordnung:* Der Entwurf eines delegierten Rechtsakts zu
„Cooperative Intelligent Transport Systems“ (C-ITS) zur Verbindung von
Fahrzeugen miteinander (Car-2-Car) und der Infrastruktur (Car-2-X) bei
dem vernetzte Verkehrsteilnehmende und die Verkehrsinfrastruktur
digitale Funknachrichten zum Verkehrsgeschehen und zum Fahrzeugzustand
austauschen, konnte bisher nicht umgesetzt werden. Es ist weiterhin
geplant EU-weite Standards zu setzen.

Im Hinblick auf Echtzeitdaten als Grundlage für multimodale Angebote für
Passagier- und Güterverkehr adressiert die Delegierte Verordnung (EU)
2017/1926 der Kommission die Bereitstellung EU-weiter multimodaler
Reiseinformationsdienste.[^12] Weitere Delegierte Verordnungen erfolgten
zu: Echtzeit-Verkehrsinformationsdiensten (EU) 2015/962, zu
straßenverkehrssicherheitsrelevanten Verkehrsinformationen (EU) Nr.
886/2013, zu Lkw-Parkplätzen (EU) Nr. 885/2013 und zu eCall-Diensten
(EU) Nr. 305/2013.

*Ankündigung zum Aufbau eines gemeinsamen europäischen Raums für
Mobilitätsdaten*

Die EU-Kommission will für den digitalen Wandel im Verkehrs- und
Mobilitätssektor die Datenverfügbarkeit, den Datenzugang und den
Austausch von Daten verbessern und Hürden, wie unklare regulatorische
Bedingungen, das Fehlen eines EU-Marktes, unterschiedliche Standards,
etc. abbauen.[^13] Aus diesem Grund wird die Kommission weitere
Maßnahmen zum Aufbau eines gemeinsamen europäischen Raums für
Mobilitätsdaten vorschlagen. Dieser sichere und vertrauenswürdige Raum
für den Austausch von Mobilitätsdaten sollte in Synergie mit anderen
Schlüsselsystemen – einschließlich Energie, Satellitennavigation und
Telekommunikation – funktionieren.[^14]

#### Datenerhebung, Datenspeicherung und Datenverwendung für das Verkehrsmanagement

§ 63e StVG regelt die von Kraftfahrzeugen regelmäßig oder
ereignisbezogen auf elektronischem Weg erhobenen und an andere Fahrzeuge
oder die informationstechnische Straßeninfrastruktur automatisiert
versendeten Daten durch den jeweils zuständigen Straßenbaulastträger
bzw. zum Verkehrsmanagement zuständigen Behörde. Verkehrsmanagement
umfasst die Erfassung der Verkehrsstärke und der sonstigen
Verkehrssituation einschließlich sicherheitsrelevanter Umfeldsituationen
sowie die unverzügliche statistische Auswertung der erfassten Daten zum
Zwecke der Verkehrslenkung sowie der Verbesserung des Verkehrsflusses
und der Verkehrssicherheit. Die Verarbeitung der Daten zu anderen
Zwecken ist unzulässig und nach Auswertung hat eine unverzügliche
Löschung zu erfolgen. Eine Auswertung liegt bspw. in der Erstellung von
Verkehrsstatistiken.

Insofern dürfte es nicht möglich sein, diese Daten für die Forschung
verfügbar zu machen.

### Mobilitätsdaten der öffentlichen Hand

#### Offene Daten des Bundes

§ 12a Abs. 1 eGovG verpflichtet Bundesbehörden unbearbeitete
maschinenlesbare Daten, die sie zur Erfüllung ihrer
öffentlich-rechtlichen Aufgaben erhoben haben (bzw. haben erheben
lassen) zum Datenabruf über öffentlich zugängliche Netze bereit – ohne
einen Anspruch auf Bereitstellung dieser Daten zu begründen. Dies gilt
u.a. nur für Daten die derart umgewandelt wurden, dass sie sich nicht
mehr auf eine identifizierte oder identifizierbare natürliche Person
beziehen oder die betroffene Person nicht oder nicht mehr identifiziert
werden kann. Weitere Ausnahmen regelt Absatz 3. Daten, die zu
Forschungszwecken erhoben wurden, sind grundsätzlich erst
bereitzustellen, wenn das der Datenerhebung zugrunde liegende
Forschungsvorhaben abgeschlossen und der Forschungszweck erfüllt ist.

Fraglich ist das Verhältnis zu § 1g Abs. 5 StVG, da § 12 Abs. 6 eGovG,
dass der Datenabruf entgeltfrei und zur uneingeschränkten
Weiterverwendung der Daten durch jedermann ermöglicht werden muss.

#### Bereitstellungspflicht von Mobilitätsdaten der Personenbeförderung im Linienverkehr und Gelegenheitsverkehr

§ 3a PBefG verpflichtet Unternehmer und Vermittler statische und
dynamische Daten sowie entsprechende Metadaten, die im Zusammenhang mit
der Beförderung von Personen im Linienverkehr und Gelegenheitsverkehr
über den Nationalen Zugangspunkt bereitzustellen.

Zum 1.07.2022 tritt die 2. Änderungsverordnung der
Mobilitätsdatenverordnung in Kraft, womit die Pflicht für
Verkehrsunternehmen dynamische Daten im Linien- und Gelegenheitsverkehr
(z.B. Störungen, Ausfälle, Verspätungen sowie die Auslastung zu
Zugangsknoten wie Bahnhöfen, Haltestellen und Haltepunkten als auch
Verfügbarkeit von Taxen oder Mietwägen im Verkehr oder an Stationen
inklusive deren Auslastung und Daten zu den abgerechneten Kosten) zur
Verfügung zu stellen konkretisiert wird. Neben dem ÖPNV werden Taxen,
Mietwagen und Poolingfahrzeuge erfasst.

#### Mobilithek

Die Mobilithek soll im Sommer 2022 den Mobilitäts Daten Marktplatz
(MDM)[^15] als Nationalen Zugangspunkt für Mobilitätsdaten ablösen und
damit Anforderungen aus den Delegierten Verordnungen zur IVS-Richtlinie
sowie des novellierten Personenbeförderungsgesetzes umsetzen. Betroffene
Daten sind etwa aus dem Bereich des öffentlichen Verkehrs oder mit
Relevanz für die Straßenverkehrssicherheit. Die Mobilithek soll eine
Plattform zum Austausch digitaler Informationen von Verkehrsbehörden,
Infrastrukturbetreiber:innen und Mobilitäts- bzw.
Informationsanbieter:innen werden und so

- als *Open Data* die Grundlage bieten, dass Informationsdienste künftig
  an einem zentralen Punkt auf Daten zurückgreifen können, um Angebote
  und Auskunftssysteme für Verkehrsteilnehmer:innen zu schaffen;

- für den Austausch von Daten mit individuellen Nutzungsrechten
  *Datenhandel* ermöglichen.[^16]

#### MobiData BW

Die Plattform stellt verkehrsträgerübergreifende Daten des Landes
Baden-Württemberg, Informationen kommunaler und regionaler Partner aus
dem ÖPNV und von privaten Mobilitätsdienstleistern zur freien
Verwendbarkeit bereit.

MobiData BW bietet derzeit unter anderem folgende Informationen:[^17]

- Fahrplandaten aus allen Nahverkehrsverbünden Baden-Württembergs

- Haltestelleninformationen aus dem baden-württembergischen Nahverkehr

- Parkrauminformationen unterschiedlicher Anbieter

- Fahrzeugstandorte und -verfügbarkeiten von Carsharing-Anbietern

- Daten zum Standort und zur Fahrradverfügbarkeit bei 131
  Fahrrad-Leihstationen in Baden-Württemberg

- In Vorbereitung befindet sich ein frei verfügbarer Routingdienst, der
  bestehende Informationen mit der Echtzeitverkehrslage auf Straße und
  Schiene kombiniert

MobiData BW stellt keine historischen oder personenbezogenen Daten zur
Verfügung.

### Fahrzeugdaten der Hersteller

#### Potentielle Auswirkungen des Entwurfs des Data Act

Nach dem Data-Act-Entwurf sollen Produkte[^18] sowie zusammenhängende
Dienstleistungen so konzipiert werden, dass die durch ihre Verwendung
erzeugten Daten dem Nutzenden standardmäßig leicht, sicher und, soweit
relevant und angemessen, direkt zugänglich sind („Data Access by
Design“).[^19] Mit Nutzer ist die natürliche oder juristische Person
gemeint, die ein Produkt besitzt, mietet oder least oder eine
Dienstleistung erhält – dies könnten auch von der Fahrer:in
personenverschiedene Eigentümer:in oder Halter:in des Fahrzeugs
sein.[^20] Weitere Regelungen betreffen ein allgemeines Recht auf
Datenübertragbarkeit an Dritte (bspw. zur Übermittlung an eine
Werkstatt) sowie Umsetzung der FRAND-Prinzipien.

Personenbezogene Daten sollen Nutzern, die nicht selbst die betroffen
Person sind, nur bei Vorliegen einer Rechtsgrundlage offengelegt werden.
Eine eigene Rechtsgrundlage schafft die Regelung nicht.[^21]

#### Verordnung (EG) Nr. 715/2007 (geändert durch die Verordnung (EU) 2018/858) über den Zugang zu Reparatur- und Wartungsdaten 

Art. 6 Abs. 1 S. 1 der VO (EG) Nr. 715/2007 verpflichtet zur Gewährung
eines uneingeschränkten und standardisierten Zugangs zu Reparatur- und
Wartungsinformationen auf leicht und unverzüglich zugängliche Weise über
das Internet mithilfe eines standardisierten Formats. Wie weitreichend
die Zugangspflichten auszulegen sind, entschied der BGH, dass ein
Automobilhersteller, der potentiellen Nutzer:innen auf seiner Website
ein Informationsportal gegen Entgelt zur Verfügung stellt, dieser
Pflicht genügt, auch wenn die Informationen nicht in elektronisch
weiterverarbeitbarer Form zur Verfügung gestellt werden.[^22] Die
EU-Kommission hat angekündigt die Regelung zum 1. Quartal 2021 zu
überprüfen, um sie für mehr Dienste zu öffnen, die Fahrzeugdaten
benötigen.[^23] Mit dem Zielhorizont zum 4. Quartal 2022 startete die
EU-Kommission eine Sondierung für einen Legislativvorschlag über den
Zugang zu Fahrzeugdaten, -funktionen und -ressourcen.[^24]

[^1]: *vzbv - Verbraucherzentrale Bundesverband*, Fahrerlos alle
    mitnehmen - automatisierte und vernetzte Mobilität aus
    Verbrauchersicht, S. 11.

[^2]: BMVI (Hrsg.), „Eigentumsordnung“ für Mobilitätsdaten? 2017, S. 20,
    abrufbar unter:
    <https://www.uni-kassel.de/fb07/index.php?eID=dumpFile&t=f&f=4043&token=5408d0e9eac3fa0fda9271f06e5d67cc84b646e8>
    (zuletzt abgerufen am: 20.06.2022).

[^3]: Statusnachrichten, um aktuelles Umgebungsbilde des Fahrzeugs,
    einschließlich anderer Verkehrsteilnehmer, zu erstellen.

[^4]: Zweck gezielt auf kritische Verkehrssituationen aufmerksam zu
    machen.

[^5]: Verordnung (EU) 2019/2144.

[^6]: Für eine differenzierte Betrachtung: *Steinrötter,* ZD 2021, 513
    (514).

[^7]: *Lutz*, DAR 2019, 125 (126), *Schmidt/Wessels,* NZV 2017, 357
    (364).

[^8]: *Steinrötter,* ZD 2021, 513 (514); kritisch: *Spiegel,* DSRITB
    2017, 691 (696).

[^9]: *Spiegel,* DSRITB 2017, 691 (697).

[^10]: *Werner/Wagner/Pieper,* RDV 2020, 111 (118); *Verbraucherzentrale
    Bundesverband,* Rechtssicher fahren mit automatisierten Fahrzeugen
    (2017), S. 6; *Spiegel,* DSRITB 2017, 691 (699).

[^11]: Richtlinie 2010/40/EU, delegierte VO (EU) 2022/670, (EU)
    2017/1926, (EU) 2015/962, (EU) Nr. 886/2013, (EU) Nr. 885/2013, (EU)
    Nr. 305/2013, vgl. auch *Barlag*, ZD-Aktuell 2016, 05421.

[^12]: *Maxian Rusche/Kotthaus/Kullak/Ruete*, in:
    Grabitz/Hilf/Nettesheim/, 75. EL Januar 2022, AEUV Art. 90 Rn. 207.

[^13]: Mitteilung der Kommission, Strategie für nachhaltige und
    intelligente Mobilität: Den Verkehr in Europa auf Zukunftskurs
    bringen, COM(2020) 789 final, Rn. 70.

[^14]: Mitteilung der Kommission, Strategie für nachhaltige und
    intelligente Mobilität: Den Verkehr in Europa auf Zukunftskurs
    bringen, COM(2020) 789 final, Rn. 71.

[^15]: Der Mobilitäts Daten Marktplatz (MDM) wird von der Bundesanstalt
    für Straßenwesen (BASt) im Auftrag des BMDV betrieben und übernimmt
    derzeit die Rolle des Nationalen Zugangspunktes.

[^16]: Informationen unter:
    <https://www.bmvi.de/SharedDocs/DE/Artikel/DG/mobilithek.html>,
    Stand 09.07.2021 (zuletzt abgerufen am 29.06.2022).

[^17]: Informationen unter: <https://www.mobidata-bw.de/> (zuletzt
    abgerufen am 29.06.2022).

[^18]: Vernetzte und intelligente Fahrzeuge fallen unter die
    vorgeschlagene Definition des Produkts, vgl. ErwGr. 14
    Data-Act-Entwurf; *Klink-Straub/Straub*, ZD-Aktuell 2022, 01076.

[^19]: *Hennemann/Steinrötter*, NJW 2022, 1481 (1483);
    *Klink-Straub/Straub*, ZD-Aktuell 2022, 01076; *Bomhard/Merkle*, RDi
    2022, 168 (173).

[^20]: *Klink-Straub/Straub*, ZD-Aktuell 2022, 01076.

[^21]: ErwGr. 24 Data-Act-Entwurf; *Hennemann/Steinrötter*, NJW 2022,
    1481 (1483).

[^22]: BGH, Urteil vom 30. Januar 2020 - I ZR 40/17.

[^23]: EU-Kommission, Eine europäische Datenstrategie, vom
    19.2.2020COM202, 66 final.

[^24]: Überblick zur öffentlichen Konsultation unter:
    <https://ec.europa.eu/info/law/better-regulation/have-your-say/initiatives/13180-Zugang-zu-Fahrzeugdaten-funktionen-und-ressourcen_de>
    (letzter Abruf 01.06.2022).
