# Metadaten TreuMoDa

## Beschreibung
Hier können wir gemeinsam eine Liste von benötigten Metadaten erstellen. Jeder soll frei im Dokument arbeiten und gerne auch Metadatenbeschreibungen verschieben, neue Kategorien hinzufügen und sortieren. Gerne könnt ihr euch schonmal gedanktlich an unserem selbst erzeugten Datensatz orientieren. Später werden wir aus dieser Liste die Metadaten für unseren Videodatensatz und für unseren Trace-Datensatz erzeugen.
Unter [Allgemeine Sammlung von Metadaten](Allgemeine-Sammlung-von-Metadaten) werden die **Kategorien** fett und die Metadaten mit Stichpunkten dargestellt. Darunter sollen auch gleich entsprechende Beispiele/ Auswahlmöglichkeiten für die Metadaten gelistet werden.
Unter [Info](Info) findet ihr zusätzliche Informationen, beispielsweise zu der Definition einer Kategorie.
Unter [Kommentare](Kommentare) können wir zu verschiedenen Fragestellungen kurze Diskussionen eröffnen und mit "erledigt" markieren bei Beendigung der Diskussion.

## Allgemeine Sammlung von Metadaten

### Technische Metadaten
- Reihenfolge der Dateien
    - einzelne, unabhängige Dateien
    - zeitlich abhängige Dateien
- Löschfrist *(Speicherdauer)*
    - 24 h
    - eine Woche
    - 2 Jahre
    - 1 Jahr
- Checksumme
    - sha256: beb5dac761422c01cebcda5408869ebd0eeaf105a73c17243bf9db56eb4b0bbf
    - md5: 50d0f071015b9ccea2507865a4dd8297
- Erstellungsdatum
    - 1970-01-01 (Format: yyyy-mm-dd)
- Dateigröße
    - 123456 (Byte)
- Dateityp (basierend auf [MIME Types](https://www.iana.org/assignments/media-types/media-types.xhtml), [short list of commonly used types](https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_types/Common_types))
    - image/png
    - text/plain
    - video/mp4
- Referenz zu Projekt/Auftrag

### Deskriptive Metadaten
- Titel
    - Consensual videos of potentially re-identifiable individuals recorded at the Autonomous Driving Test Area Baden-Württemberg (raw images with location and IMU data)
- Schlagworte/Subject ([Dublin Core Subject](http://purl.org/dc/elements/1.1/subject))
    - traffic, individuals, anonymization, pedastrians, cyclists, autonomous driving, privacy
- Projektname
    - TreuMoDa - Treuhandstelle für Mobilitätsdaten
- Projektbeschreibung
- Projektwebsite (wenn vorhanden)
    - https://www.treumoda.de
- Publisher (von [Datacite](https://support.datacite.org/docs/schema-40))
    - RADAR4KIT
    - Zenodo
- Veröffentlichungsdatum:
    - 1970-01-02
- Resource Type (von [Datacite](https://support.datacite.org/docs/schema-40))
    - Dataset/Video and IMU data
- Sprache
    - Englisch
    - Deutsch
- Identifier des Datensatzes
    - DOI: 10.5281/zenodo.7805961
    - Interne ID (alternativ)
- Umgebung
    - Stadt
    - Land
    - Autobahn
    - Ort der Datenerhebung
- Geo-Koordinaten der Daten (entweder einzelner Punkt oder Bereich), z. B. wie in http://schema.org/GeoCoordinates
   
### Administrative Metadaten
- Weitergabe als
    - Testdaten
    - Stichprobe
    - Hauptdatensatz

### Legale Metadaten

- Lizenz:
    - CC BY-SA 4.0
    - CC BY-NC-ND 4.0
    - Creative Commons Attribution Non Commercial Share Alike 4.0 International

- Verwendungszweck Datennehmer
    - Forschung
        - Mobilitätszwecke
        - autonomes Fahren
        - Trainingsdaten KI
    - Weiterentwicklung eines Produktes
        - Trainingsdaten KI
    - (Werbung) *(Negativbeispiel)*
- zulässige Verwendungszwecke (string) 
    - *Beschreibung: Aufzählung der Verwendungszwecke aus der Lizenz*
    - Weitergabe an Dritte
    - Weitergabe an Forschung für...

- Sicherungsmaßnahmen nach Art. 89 DSGVO 
    - Pseudonymisierung
    - Ethikkommission
    - Anonymisierung
    - *Beschreibung: Leistung von DN notwendig*

- Erhebungszweck(e)
    - Produktverbesserung
    - Nutzerinteraktion
    - (usw.)
- Datennehmer beschränkt?
    - true/false
- erlaubte Datennehmer
    - stat. Ämter
    - Automobilhersteller
- nicht erlaubte Datennehmer
    - Mobilitätsdienstleister
- Datenschutz
    - Datenschutz-Grundverordnung (EU) 2016/679 (DSGVO)
    - Bundesdatenschutzgesetz (BDSG)
    - Landesdatenschutzgesetz 

- rechtl. Grundlage zur Datenerhebung
    - Art. 6 Abs. 1 UAbs. 1 lit. a DSGVO (Einwilligungstext sollte verlinkt sein) 
    - Art. 6 Abs. 1 UAbs. 1 lit. c iVm (Quelle der rechtlichen Verpflichtung)
    - Art. 6 Abs. 1 UAbs. 1 lit. f (Vorgebrachte Interessen sollten irgendwo aufgelistet werden)
    - Art. 9 Abs. 2 lit. a; Art. 6 Abs. 1 UAbs. 1 lit. a (Bsp. bei biometrischen Daten)
- Link zu Einwilligungstext (falls Rechtsgrundlage Art. 6 Abs. 1 UAbs. 1 lit. a DSGVO; optional: konkrete Einwilligungen verlinken, aber eher für DG interessant)
- Bes. kat. pb Daten enthalten?
    - true/false
    - *true erzwingt Kategorien, Rechtsgrundlage nach Art. 9 Abs. 2* &rarr; *folgendes Metadatum*
- Besondere Kategorien pb Daten nach Art. 9 DSGVO
    - *wörtlich aus Art. 9 Abs. 1 DSGVO übernommen, abgeschlossener Katalog*
    - Rassische Herkunft
    - Ethnische Herkunft
    - Politische Meinungen 
    - Religiöse Überzeugungen
    - Weltanschauliche Überzeugungen
    - Gewerkschaftsangehörigkeit
    - Genetische Daten
    - Biometrische Daten zur eindeutigen Identifizierung einer natürlichen Person
    - Gesundheitsdaten
    - Daten zu Sexualleben
    - Daten zur sexuellen Orientierung einer natürlichen Person 

- rechtl. Grundlage der Weiterverarbeitung 
 
- Schutzrechte
    - UrhG
        - Urheberrecht
        - Sui-Generis-Schutz
        - Datenbankwerk
    - GeschGehG: Gesetz zum Schutz von Geschäftsgeheimnissen

- notwendige technisch-organisatorische Maßnahmen
    - Separater Host
    - Zugangsberechtigungen
    - Sofortige Löschung
    - Spezielle Schulung
    - NDAs (Geheimhaltungsvereinbarungen)
    - (usw.)
    - redundante Speicherung

    
### Provenienzmetadaten/Herkunftsmetadaten 
- Ort
    - Karlsruhe, Deutschland
    - Geo-Koordinaten?
- Datum (der Datenerhebung)
    - 2023-03-30 (Einzeldatum)
    - 1970-01-01 -- 1970-02-01 (Zeitraum)
- Creator
    - John Doe
        - ORCID: 0000-0000-0000-0000
        - Affiliation: KIT
- Datenverantwortliche Institution
    - KIT
    - Mercedes-Benz
- Contributor
    - - John Doe
        - ORCID: 0000-0000-0000-0000


### Prozessmetadaten
- Datenaufbereitung/Source ([Dublin Core](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/elements11/source/))
    - Original Datensatz
    - Wenn Daten nicht erfasst wurden: Verweis mit DOI auf Originaldaten
- Anonymisierungsverfahren:
    - Bluring
    - Deep Natural Anonymization
    - Schwärzen
    - Zeitliche/örtliche Entkopplung
- Parameter des Anonymisierungsverfahrens

### Strukturelle Metadaten
- Relation zu anderen Ressourcen
    - html: "https://zenodo.org/record/7805961"
    - doi: "https://doi.org/10.5281/zenodo.7805961"

### Fachspezifische Metadaten
- Auflösung der Videos
    - 1920x1080
- Abtastrate Video
    - 10 fps
- Dauer der Aufnahmen
    - 1min 10sek
- Verkehrsszenarien
    - Abbiegende Fahrzeuge
    - Abbiegende Fahrradfahrer:innen
    - Querende Fußgänger:innen
- Fahrmanöver
    - Linksabbiegevorgang
- Lichtverhältnisse auf Video
    - Im Hellen und im Dunkeln
- Personenbezogene Daten
    - Fahrzeugkennzeichen
    - Gesichter
    - Besondere Merkmale (Hut etc.)
    - Traces


## Info

Zur Findung von Kategorien wuden bisher folgende Quellen genutzt:
>[name=Vivien Geenen]
>[time=Mo., 24. Juli 2023]
https://www.izus.uni-stuttgart.de/fokus/engmeta/
https://www.computerweekly.com/de/definition/Metadaten
 
> [name=Constantin] Vorschlag zu Schema: https://specs.frictionlessdata.io/table-schema/
> Wird u.a. von FAIR Data Spaces zu Grunde gelegt, um automatisch die Qualität von Daten auszuwerten: https://github.com/FAIR-DS4NFDI/FAIR-DSWiki/wiki/WP3-WP4-Technical-Foundations-and-Demonstrators#wp-42---fair-ds-demonstrator-data-quality-assurance
> Kann nicht einschätzen, ob das technisch sinnvoll ist. Sieht aber nett aus.
>[name=Vivien]
>Reminder zu Link:
>https://support.datacite.org/docs/datacite-metadata-schema-v44-properties-overview
>https://www.dublincore.org/specifications/dublin-core/dces/
>[name=Peter]
>https://schema.org/ 


